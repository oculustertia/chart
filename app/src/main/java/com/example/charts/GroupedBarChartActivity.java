package com.example.charts;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.List;

public class GroupedBarChartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_bar_chart);

        //---------------Chart declaration
        BarChart barChart = (BarChart)findViewById(R.id.groupedbarchart1);
        BarChart barChart2 = (BarChart)findViewById(R.id.groupedbarchart2);
        //

        //----------------------data
        float groupSpace = 0.04f;
        float barSpace = 0.02f; // x2 dataset
        float barWidth = 0.46f; // x2 dataset
        // (0.46 + 0.02) * 2 + 0.04 = 1.00 -> interval per "group"


        Description d = new Description();
        d.setText("");
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setDescription(d);
        barChart.setMaxVisibleValueCount(50);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);

        XAxis xl = barChart.getXAxis();
        xl.setGranularity(1f);
        xl.setCenterAxisLabels(true);


        YAxis leftAxis = barChart.getAxisLeft();

        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(30f);
        //leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true
        barChart.getAxisRight().setEnabled(false);

       int startYear = 1;
       //int endYear = 4;



        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        List<BarEntry> yVals2 = new ArrayList<BarEntry>();

        yVals1.add(new BarEntry(1f, 22));
        yVals2.add(new BarEntry(1f, 28));
        yVals1.add(new BarEntry(2f, 22));
        yVals2.add(new BarEntry(2f, 32));
        yVals1.add(new BarEntry(3f, 41));
        yVals2.add(new BarEntry(3f, 54));
        yVals1.add(new BarEntry(4f, 65));
        yVals2.add(new BarEntry(4f, 76));
        yVals1.add(new BarEntry(5f, 65));
        yVals2.add(new BarEntry(5f, 76));
        yVals1.add(new BarEntry(6f, 65));
        yVals2.add(new BarEntry(6f, 76));

        BarDataSet set1, set2;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet)barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet)barChart.getData().getDataSetByIndex(1);
            set1.setValues(yVals1);
            set2.setValues(yVals2);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            // create 2 datasets with different types
            set1 = new BarDataSet(yVals1, "Success");
            set1.setColor(Color.rgb(104, 241, 175));
            set2 = new BarDataSet(yVals2, "Failure");
            set2.setColor(Color.rgb(164, 228, 251));

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);
            dataSets.add(set2);

            BarData data = new BarData(dataSets);
            barChart.setData(data);
        }
//        barChart.setDragEnabled(true);
//        barChart.setMaxVisibleValueCount(3);
        barChart.setVisibleXRangeMaximum(4);
        barChart.animateY(1000);

        //barChart.moveViewToX(10);


        barChart.getBarData().setBarWidth(barWidth);
        barChart.getXAxis().setAxisMinValue(startYear);
        barChart.groupBars(startYear, groupSpace, barSpace);
        barChart.invalidate();

        //---------------------------------------------------------------


        barChart2.setDrawBarShadow(false);
        barChart2.setDrawValueAboveBar(true);
        barChart2.setDescription(d);
        barChart2.setMaxVisibleValueCount(50);
        barChart2.setPinchZoom(false);
        barChart2.setDrawGridBackground(false);

        XAxis xl2 = barChart2.getXAxis();
        xl2.setGranularity(1f);
        xl2.setCenterAxisLabels(true);


        YAxis leftAxis2 = barChart2.getAxisLeft();

        leftAxis2.setDrawGridLines(false);
        leftAxis2.setSpaceTop(30f);
        //leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true
        barChart2.getAxisRight().setEnabled(true);

        int startYear2 = 0;
        //int endYear = 4;



        List<BarEntry> yVals12 = new ArrayList<BarEntry>();
        List<BarEntry> yVals22 = new ArrayList<BarEntry>();

        yVals12.add(new BarEntry(1f, 22));
        yVals22.add(new BarEntry(1f, 28));
        yVals12.add(new BarEntry(2f, 22));
        yVals22.add(new BarEntry(2f, 32));
        yVals12.add(new BarEntry(3f, 41));
        yVals22.add(new BarEntry(3f, 54));
        yVals12.add(new BarEntry(4f, 65));
        yVals22.add(new BarEntry(4f, 76));
        yVals12.add(new BarEntry(5f, 65));
        yVals22.add(new BarEntry(5f, 76));
        yVals12.add(new BarEntry(6f, 65));
        yVals22.add(new BarEntry(6f, 76));

        BarDataSet set12, set22;

        if (barChart2.getData() != null && barChart2.getData().getDataSetCount() > 0) {
            set12 = (BarDataSet)barChart2.getData().getDataSetByIndex(0);
            set22 = (BarDataSet)barChart2.getData().getDataSetByIndex(1);
            set12.setValues(yVals12);
            set22.setValues(yVals22);
            barChart2.getData().notifyDataChanged();
            barChart2.notifyDataSetChanged();
        } else {
            // create 2 datasets with different types
            set12 = new BarDataSet(yVals12, "Sucess");
            set12.setColor(Color.rgb(20, 10, 15));
            set22 = new BarDataSet(yVals22, "Failure");
            set22.setColor(Color.rgb(80, 100, 115));

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set12);
            dataSets.add(set22);

            BarData data = new BarData(dataSets);
            barChart2.setData(data);
        }
//        barChart.setDragEnabled(true);
//        barChart.setMaxVisibleValueCount(3);\

        //This provide horizontal scrollability
        barChart2.setVisibleXRangeMaximum(4);

        //This set the Y animation  in milli sec
        barChart2.animateY(1000);

        // X-Axis Values
        String[] months = new String[] {"Jan", "Feb", "March", "April","May","June"};
        xl2.setValueFormatter(new IndexAxisValueFormatter(months));
        barChart2.getAxisLeft().setAxisMinimum(0);

        //X-Axis position
        xl2.setPosition(XAxis.XAxisPosition.BOTTOM);
        //barChart.moveViewToX(10);


        barChart2.getBarData().setBarWidth(barWidth);
        barChart2.getXAxis().setAxisMinimum(startYear2);
        barChart2.groupBars(startYear2, groupSpace, barSpace);
        barChart2.invalidate();

    }
}
